import React from "react";
import Header from '../header';
import Menu from '../menu';
import Footer from '../footer';

const App = () => {
    return (
        <>
            <Header />
            <Menu />
            <Footer />
        </>
    )
}

export default App;