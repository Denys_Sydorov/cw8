import React, {useState, useEffect} from "react";
import './menu.css';
import Service from '../services';

const Menu = () => {

    const menuInf = new Service()  
    const [cart, setCart] = useState(menuInf.getMenu())   

return (
    <>
    {cart === undefined ? <p>Загрузка</p> : <div className='carts'>{cart.filter((e) => {
        return e.stopList === false
    }).map((item, i) => {
        return <div className='cart'>
            <div><img src={item.productImageUrl}></img></div>
            <div>{item.productName}</div>
            <div>{item.price}</div>
            <button>Купить</button>
        </div>
    })}</div> } 
    </>
)
}
export default Menu;